<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Ingredient Categories
	|--------------------------------------------------------------------------
	|
	*/

	'category_animal_origin' => 'Origine animală',
	'category_meat' => 'Carne',
	'category_dairy' => 'Lactate',
	'category_red_meat' => 'Carne roşie',
	'category_poultry' => 'Păsări',
	'category_pork' => 'Porc',
	'category_fish' => 'Peşte',
	'category_other_seafood' => 'Fructe de mare',
	'category_plant_origin' => 'Origine vegetală',
	'category_vegetables' => 'Legume',
	'category_fruit' => 'Fructe',
	'category_grain' => 'Cereale',
	'category_mushrooms' => 'Ciuperci',
	'category_rice' => 'Orez',
	'category_seasonings' => 'Condimente',

];
