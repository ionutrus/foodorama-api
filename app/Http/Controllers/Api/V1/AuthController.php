<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginPost;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	/**
	 * Login user and create token
	 *
	 * @param [string] email
	 * @param [string] password
	 *
	 * @return [string] access_token
	 * @return [string] token_type
	 * @return [string] expires_at
	 */
	public function login(LoginPost $request)
	{
		$credentials = $request->only([
			'email',
			'password'
		]);

		if(!Auth::attempt($credentials)){
			return response()->json([
				'message' => 'Invalid credentials'
			], 422);
		}

		$user = $request->user();

		$tokenResult = $user->createToken('PersonalAccessToken');
		$token = $tokenResult->token;
		$token->save();

		return response()->json([
			'access_token'=> $tokenResult->accessToken,
			'token_type' => 'Bearer',
			'expires_at' => Carbon::parse(
				$tokenResult->token->expires_at
			)->toDateTimeString(),
			'user' => $user
		]);
	}

	/**
	 * Logout user(Revoke the token)
	 *
	 * @return [string] message
	 */
	public function logout(Request $request)
	{
		$request->user()->token()->revoke();
		return response()->json([
			'message'=>'Successfully logged out'
		]);
	}
}
