<?php

namespace App\Http\Controllers\Api\V1;

use App\Foodorama\Managers\IngredientCategoryManager;
use App\Http\Requests\IngredientCategoryRequest;
use App\IngredientCategory;
use App\Http\Resources\IngredientCategory as IngredientCategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IngredientCategoryController extends Controller
{
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Http\Requests\IngredientCategoryRequest  $request
	 * @param  \App\IngredientCategory  $ingredientCategory
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(IngredientCategoryRequest $request, IngredientCategory $category)
	{
		$category->delete();

		return response()->json(null, 204);
	}

	/**
	 * Get the data in tree form.
	 *
	 * @param  \App\Http\Requests\IngredientCategoryRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function getTree(IngredientCategoryRequest $request)
	{
		return response()->json(IngredientCategoryManager::getDataAsTree());
	}

	/**
	 * Get the selectable data in tree form.
	 *
	 * @param  \App\Http\Requests\IngredientCategoryRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function getSelectableTree(IngredientCategoryRequest $request)
	{
		return response()->json(IngredientCategoryManager::getDataAsTree(true));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Http\Requests\IngredientCategoryRequest  $request
	 * @param  \App\IngredientCategory  $ingredientCategory
	 * @return \Illuminate\Http\Response
	 */
	public function show(IngredientCategoryRequest $request, IngredientCategory $category)
	{
		return response()->json(new IngredientCategoryResource($category));
	}

    /**
     * Store a newly created resource in storage.
     *
	 * @param  \App\Http\Requests\IngredientCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IngredientCategoryRequest $request)
    {
        return response()->json(new IngredientCategoryResource(factory(IngredientCategory::class)->create(array_merge(
			$request->all(),
			[
				'created_by' => auth()->id()
			]
		))), 201);
    }

    /**
     * Update the specified resource in storage.
     *
	 * @param  \App\Http\Requests\IngredientCategoryRequest  $request
     * @param  \App\IngredientCategory  $ingredientCategory
     * @return \Illuminate\Http\Response
     */
    public function update(IngredientCategoryRequest $request, IngredientCategory $category)
    {
        $category->update($request->all());

		return response()->json(new IngredientCategoryResource($category));
    }
}
