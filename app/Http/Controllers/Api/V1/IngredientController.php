<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\OnTranslationTableChanged;
use App\Foodorama\Managers\TranslationManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\IngredientRequest;
use App\Ingredient;
use App\Http\Resources\Ingredient as IngredientResource;
use Illuminate\Http\Request;
use Stichoza\GoogleTranslate\GoogleTranslate;

class IngredientController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\IngredientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IngredientRequest $request)
    {
		$tr = new GoogleTranslate('en');
		$tr->setSource('ro');

		$tManager = new TranslationManager('ro');

    	$text = $request->get('text');
    	$generatedKey = 'ingredient_' . str_replace(' ', '_', strtolower($tr->translate($text)));

    	// check if generated key already exists
		if (Ingredient::whereLangKey($generatedKey)->exists()) {
			return response()->json([
				'message' => 'The given data was invalid.',
				'errors' => [
					"lang_key" => [
						"The ingredient already exists in the database"
					]
				]
			], 422);
		}

		$tManager->add([
			'group' => 'ingredients',
			'key' => $generatedKey,
			'value' => $text
		]);

		$tManager->setLocale('en');
		$tManager->add([
			'group' => 'ingredients',
			'key' => $generatedKey,
			'value' => $tr->translate($text)
		]);

		// save the ingredient to db
		$ingredient = factory(Ingredient::class)->create(
			array_merge(
				$request->except('text'),
				[
					'created_by' => auth()->id(),
					'lang_key' => $generatedKey
				]
		));

		// dispatch event
		event(new OnTranslationTableChanged());

		return response()->json(new IngredientResource($ingredient), 201);
    }

    /**
     * Display the specified resource.
     *
	 * @param  \App\Http\Requests\IngredientRequest  $request
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function show(IngredientRequest $request, Ingredient $ingredient)
    {
        return response()->json(new IngredientResource($ingredient));
    }

    /**
     * Update the specified resource in storage.
     *
	 * @param  \App\Http\Requests\IngredientRequest  $request
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function update(IngredientRequest $request, Ingredient $ingredient)
    {
        $ingredient->update($request->except('translations'));

        if ($request->has('translations')) {
        	$translationTableChanged = false;
        	$translationManager = new TranslationManager();

        	foreach ($request->get('translations') as $locale => $translationData) {
        		$row = $translationManager->updateByLocaleAndKey($locale, $ingredient->lang_key, [
        			'value' => $translationData['text']
				]);

        		if (!is_null($row)) {
        			$translationTableChanged = true;
				}
			}

			if ($translationTableChanged) {
				// dispatch event
				event(new OnTranslationTableChanged());
			}
		}

		return response()->json(new IngredientResource($ingredient));
    }

    /**
     * Remove the specified resource from storage.
     *
	 * @param  \App\Http\Requests\IngredientRequest  $request
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function destroy(IngredientRequest $request, Ingredient $ingredient)
    {
        $ingredient->delete();

		// dispatch event
		event(new OnTranslationTableChanged());

        return response()->json(null, 204);
    }

    public function getTranslations(IngredientRequest $request, Ingredient $ingredient)
	{
		return response()->json($ingredient->translations);
	}
}
