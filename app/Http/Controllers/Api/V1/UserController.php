<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getLoggedInUser()
	{
		return response()->json(auth()->user());
	}
}
