<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class IngredientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
    	$rules = [];

    	if ($request->method() === 'POST') {
    		$rules = [
				'ingredient_category_id' => 'required|numeric|exists:ingredient_categories,id',
				'text' => 'required|string'
			];
		} elseif ($request->method() === 'PATCH') {
			$rules = [
				'ingredient_category_id' => 'nullable|numeric|exists:ingredient_categories,id',
				'translations' => 'array|nullable',
				'translations.*.text' => 'required|string'
			];
		}

        return $rules;
    }
}
