<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class IngredientCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return in_array($request->method(), [
			'POST', 'PATCH'
		]) ? [
            'parent_id' => 'nullable|numeric|exists:ingredient_categories,id',
			'lang_key' => 'required|string|unique:ingredient_categories'
        ] : [];
    }
}
