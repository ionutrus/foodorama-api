<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;

class Ingredient extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$data = parent::toArray($request);
		$data['name'] = (Lang::has("ingredients.{$data['lang_key']}"))? trans("ingredients.{$data['lang_key']}") : $data['lang_key'];
		unset($data['lang_key']);

		return $data;
    }
}
