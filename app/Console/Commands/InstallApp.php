<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'foodapi:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs Food-O-Rama API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->call('migrate:fresh');
		$this->call('db:seed');
		$this->call('passport:client', ['--password' => true]);
		$this->call('passport:client', ['--personal' => true]);
	}
}
