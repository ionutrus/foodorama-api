<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = [
		'lang_key'
	];

    protected $hidden = [
    	'created_by',
	];

	public function author()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function category()
	{
		return $this->belongsTo('App\IngredientCategory', 'ingredient_category_id', 'id');
	}

	public function translations()
	{
		return $this->hasMany('App\Translation', 'key', 'lang_key');
	}
}
