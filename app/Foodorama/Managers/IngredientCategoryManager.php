<?php

namespace App\Foodorama\Managers;

use App\IngredientCategory;

class IngredientCategoryManager {

	public static function getDataAsTree($selectable = false)
	{
		$exportData = [];
		$categories = ($selectable) ? IngredientCategory::doesntHave('children')->get() : IngredientCategory::whereNull('parent_id')->get();

		foreach ($categories->load('children') as $category) {
			$children = [];

			foreach ($category->children as $subCategory) {
				$subCategoryChildren = [];

				foreach ($subCategory->children as $finalCategory) {
					$subCategoryChildren[] = ($selectable) ? [
						'id' => $finalCategory->id,
						'name' => $finalCategory->name
					] : [
						'id' => $finalCategory->id,
						'name' => $finalCategory->name,
						'children' => []
					];
				}

				$children[] = ($selectable) ? [
					'id' => $subCategory->id,
					'name' => $subCategory->name
				] : [
					'id' => $subCategory->id,
					'name' => $subCategory->name,
					'children' => $subCategoryChildren
				];
			}


			$exportData[] = ($selectable) ? [
				'id' => $category->id,
				'name' => $category->name
			] : [
				'id' => $category->id,
				'name' => $category->name,
				'children' => $children
			];
		}

		return $exportData;
	}

}