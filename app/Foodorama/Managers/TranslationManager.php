<?php

namespace App\Foodorama\Managers;

use App\Translation;
use Illuminate\Support\Facades\Config;
use App\Foodorama\Export\TranslationExport;

class TranslationManager {

	protected $locale;

	public function __construct($locale = null)
	{
		$this->setLocale($locale);
	}

	public function setLocale($locale = null)
	{
		if (is_null($locale)) {
			$locale = Config::get('app.locale');
		}

		$this->locale = $locale;
	}


	public function add(array $data = [])
	{
		return factory(Translation::class)->create(array_merge(
			[
				'locale' => $this->locale
			],
			$data
		));
	}

	public function updateByLocaleAndKey($locale = null, $key, array $data = [])
	{
		$locale = (!is_null($locale)) ? $locale : $this->locale;

		$row = Translation::where('locale', $locale)
				->where('key', $key)
				->first();

		if (!is_null($row) && $row instanceof Translation) {
			$row->update($data);
		}

		return $row;
	}

	public function delete($where = [])
	{
		return Translation::where($where)->delete();
	}

	public function export($languages = ['*'], $groups = ['*'])
	{
		$exportManager = new TranslationExport($languages, $groups);
		$exportManager->run();
	}
}