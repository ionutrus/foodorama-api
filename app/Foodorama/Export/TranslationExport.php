<?php

namespace App\Foodorama\Export;

use App\Translation;

class TranslationExport {

	protected $content;

	protected $data;

	protected $file;

	protected $files;

	protected $groups;

	protected $languages;

	public function __construct($languages = ['*'], $groups = ['*'])
	{
		$this->setLanguages($languages);
		$this->setGroups($groups);
		$this->setFilesToExport();
	}

	public function run()
	{
		// check if all folders exist, if not create them
		foreach ($this->languages as $lang) {
			if (!file_exists(resource_path() . DIRECTORY_SEPARATOR . "lang" . DIRECTORY_SEPARATOR . $lang)) {
				mkdir(resource_path() . DIRECTORY_SEPARATOR . "lang" . DIRECTORY_SEPARATOR . $lang);
			}
		}

		// loop over the files to be generated
		foreach ($this->files as $fileData) {
			// if the file is already exported, delete it

			// set file
			$this->file = $fileData;

			// fetch the data to be exported
			$translations = Translation::where('group', $this->file['group'])
								->where('locale', $this->file['lang'])
								->get();

			$this->setData($translations);

			$this->setContent();

			$this->writeFile();
		}

	}

	protected function setContent()
	{
		$this->content = '<?php ' . "\n";
		$this->content .= 'return ' . var_export($this->data, true) . ';';
	}

	protected function setData($translations)
	{
		$this->data = [];

		foreach ($translations as $translation) {
			$this->data[$translation['key']] = $translation['value'];
		}
	}

	protected function setFile($file)
	{
		$this->file = $file;
	}

	protected function setFilesToExport()
	{
		$this->files = [];

		foreach ($this->languages as $lang) {
			foreach ($this->groups as $group) {
				$this->files[] = [
					'group' => $group,
					'lang' => $lang,
					'path' => resource_path() . DIRECTORY_SEPARATOR . "lang" . DIRECTORY_SEPARATOR . "{$lang}" . DIRECTORY_SEPARATOR . "{$group}.php"
				];
			}
		}
	}

	protected function setGroups($groups = ['*'])
	{
		if ($groups == ['*']) {
			$groups = [];

			$data = Translation::all()
				->groupBy('group')
				->toArray();

			foreach ($data as $group => $values) {
				$groups[] = $group;
			}
		}

		$this->groups = $groups;
	}

	protected function setLanguages($languages = ['*'])
	{
		if ($languages == ['*']) {
			$languages = [];

			$data = Translation::all()
						->groupBy('locale')
						->toArray();

			foreach ($data as $lang => $values) {
				$languages[] = $lang;
			}
		}

		$this->languages = $languages;
	}

	protected function writeFile()
	{
		file_put_contents($this->file['path'], $this->content);
	}
}