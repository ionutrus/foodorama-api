<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class IngredientCategory extends Model
{
    protected $fillable = [
    	'parent_id',
		'lang_key'
	];

    protected $hidden = [
    	'created_by',
    	'parent_id'
	];

	public function author()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function children()
	{
		return $this->hasMany('App\IngredientCategory', 'parent_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo('App\IngredientCategory', 'parent_id', 'id');
	}

	public function getNameAttribute()
	{
		return (Lang::has("ingredient_categories.{$this->lang_key}")) ? trans("ingredient_categories.{$this->lang_key}") : $this->lang_key;
	}
}
