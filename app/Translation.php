<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'locale',
		'group',
		'key',
		'value'
	];
}
