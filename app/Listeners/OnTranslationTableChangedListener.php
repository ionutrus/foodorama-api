<?php

namespace App\Listeners;

use App\Events\OnTranslationTableChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Foodorama\Export\TranslationExport;

class OnTranslationTableChangedListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OnTranslationTableChanged  $event
     * @return void
     */
    public function handle(OnTranslationTableChanged $event)
    {
        $exportManager = new TranslationExport();
        $exportManager->run();
    }
}
