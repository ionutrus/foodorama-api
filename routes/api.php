<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
	'prefix' => 'v1',
	'middleware' => ['guest']
], function() {
	Route::post('login', 'Api\V1\AuthController@login');
});

Route::group([
	'prefix' => 'v1',
	'middleware' => ['auth:api']
], function() {
	Route::get('user', 'Api\V1\UserController@getLoggedInUser');

	Route::get('ingredients/categories/tree', 'Api\V1\IngredientCategoryController@getTree');
	Route::get('ingredients/categories/tree/selectable', 'Api\V1\IngredientCategoryController@getSelectableTree');
	Route::post('ingredients/categories', 'Api\V1\IngredientCategoryController@store');
	Route::get('ingredients/categories/{category}', 'Api\V1\IngredientCategoryController@show');
	Route::patch('ingredients/categories/{category}', 'Api\V1\IngredientCategoryController@update');
	Route::delete('ingredients/categories/{category}', 'Api\V1\IngredientCategoryController@destroy');

	Route::post('ingredients', 'Api\V1\IngredientController@store');
	Route::get('ingredients/{ingredient}', 'Api\V1\IngredientController@show');
	Route::get('ingredients/{ingredient}/translations', 'Api\V1\IngredientController@getTranslations');
	Route::patch('ingredients/{ingredient}', 'Api\V1\IngredientController@update');
	Route::delete('ingredients/{ingredient}', 'Api\V1\IngredientController@destroy');

	Route::get('logout', 'Api\V1\AuthController@logout');
});