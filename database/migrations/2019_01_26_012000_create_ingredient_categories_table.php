<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_categories', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedBigInteger('created_by');
			$table->unsignedInteger('parent_id')->nullable();
            $table->timestamps();
			$table->string('lang_key')->unique();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('parent_id')->references('id')->on('ingredient_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredient_categories');
    }
}
