<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(App\User::class)->create([
			'first_name' => 'Ionuţ',
			'last_name' => 'Ruş',
			'email' => 'pixelangello@gmail.com',
			'password' => Hash::make('1234')
		]);
    }
}
