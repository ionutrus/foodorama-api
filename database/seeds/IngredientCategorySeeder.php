<?php

use App\User;
use Illuminate\Database\Seeder;
use App\IngredientCategory;

class IngredientCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$author = User::whereEmail('pixelangello@gmail.com')->first();

        $animalOrigin = factory(IngredientCategory::class)->create([
        	'created_by' => $author->id,
			'lang_key' => 'category_animal_origin'
		]);

        $meat = factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $animalOrigin->id,
			'lang_key' => 'category_meat'
		]);

		$dairy = factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $animalOrigin->id,
			'lang_key' => 'category_dairy'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $meat->id,
			'lang_key' => 'category_red_meat'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $meat->id,
			'lang_key' => 'category_poultry'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $meat->id,
			'lang_key' => 'category_pork'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $meat->id,
			'lang_key' => 'category_fish'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $meat->id,
			'lang_key' => 'category_other_seafood'
		]);

		$plantOrigin = factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'lang_key' => 'category_plant_origin'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $plantOrigin->id,
			'lang_key' => 'category_vegetables'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $plantOrigin->id,
			'lang_key' => 'category_fruit'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $plantOrigin->id,
			'lang_key' => 'category_grain'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $plantOrigin->id,
			'lang_key' => 'category_mushrooms'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $plantOrigin->id,
			'lang_key' => 'category_rice'
		]);

		factory(IngredientCategory::class)->create([
			'created_by' => $author->id,
			'parent_id' => $plantOrigin->id,
			'lang_key' => 'category_seasonings'
		]);
    }
}
