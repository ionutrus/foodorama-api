<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Config;

$factory->define(App\Translation::class, function (Faker $faker) {
    return [
        'locale' => Config::get('app.locale'),
		'group' => 'messages'
    ];
});
